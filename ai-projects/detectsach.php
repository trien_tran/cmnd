<?php include('../header.php') ?>  
<body class="royal_preloader">  
  
  <div id="royal_preloader"></div>
  
  <!-- MENU
    ================================================== -->  

    <nav id="menu-wrap" class="menu-back cbp-af-header">
      <div class="parallax-pattern" style="background-image: url('images/parallax-pattern.jpg')"></div>
      <?php include('../menu-top.php') ?>
    </nav>
  
    
  <!-- Primary Page Layout
  ================================================== -->

    <div class="section-block small-height">
        <div class="parallax" style="background-image: url('https://www.macobserver.com/wp-content/uploads/2018/04/AI-concept-human-head-1200x632.jpg')"></div>
        <div class="dark-over-hero"></div>
        
        <div class="home-text-freelance project-hero-margin z-bigger">
          <div class="container fade-elements">
            <div class="twelve columns">
              <h2>Nhận diện đối tượng sách trong hình ảnh</h2>
            </div>
          </div>
        </div>
          
    </div>
    <br>


    <div>
      <p style="font-size: 24px ;text-align: center;">1. Giới thiệu về ứng dụng:</p>
    <br>
    <br>
      <p style="font-size: 24px;text-align: center;">2. Hình ảnh/Video mô phỏng kết quả:</p>
      <br>
      <br>
      <br>
      <iframe style="margin: 0 auto; display: block;" width="450" height="283" src="video_demo/demo_sach.mp4"  frameborder="0" allowfullscreen wmode="Opaque"></iframe>
      <br>
      <br>
    <br>
    <br>
      <p style="font-size: 24px;text-align: center;">3. Kiểm thử ứng dụng</p>
    </div>
    
    <div class="section-block padding-top-bottom">
        <!-- <div class="container"> -->
            
      <!-- http://media.anhp.vn:8081/files/nganpham/NGAN_501527388674.jpg -->
    <div id="form-train-detect">
      <div class="container khung">
        <div class="loading-image">
          <div id="ajaxLoader-inner"></div>
          <div class="image-show">
            <img id="img" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Missing_image_icon_with_camera_and_upload_arrow.svg/1109px-Missing_image_icon_with_camera_and_upload_arrow.svg.png" alt="Snow" style="width:100%">
          </div>
        </div>
        
      </div>
            <script>
              function getLabel(id) {
                alert($(`#btn${id}`).val())
              }
            </script>

            <div class="container1">
              <form method="post" action="/projects/upload.php" enctype="multipart/form-data" id="myform">
                  <div >
                      <input type="file" id="file" name="file"/><br><br>
          <!--             <input type="file" id="file" name="file" /> -->
                      <input style="width: 200px" type="button" class="button" value="Upload" id="but_upload">
                  </div>
              </form>
          </div>
        <script>
            $(document).ready(function(){
                $("#but_upload").click(function(){
                  var fd = new FormData();
                  var files = $('#file')[0].files[0];
                  fd.append('file',files);
                  $(".btn-detect-img").hide();

                  $.ajax({
                      url: 'upload.php',
                      type: 'post',
                      data: fd,
                      contentType: false,
                      processData: false,
                      beforeSend: function () { // traitements JS à faire AVANT l'envoi
                          $('#ajaxLoader-inner')
                          .html('<img src="https://cdn.dribbble.com/users/178981/screenshots/2245419/xxxx.gif" /> <span>Vui lòng đợi...</span>'); // add a gif loader  
                      },
                      success: function(response){
                          if(response != 0){
                                                 
                              $("#img").attr("src",response); 
                              $(".preview img").show(); // Display image element
                              var url = 'http://127.0.0.1:8080/image/';
                              // var getUrl = window.location;
                              // var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
                              // var link = baseUrl + "projects/" + response;
                              var link = response;

                              console.log(response);

                              $.get(url + link , function(data) {
                                $('#ajaxLoader-inner img, #ajaxLoader-inner span').fadeOut(0000, function () {})
                                  for (var i = 0; i < data.length; i++){
                                      var r='<input  type="button" class = "btn-detect-img" id = "btn'+i+'" onclick="getLabel('+i+')" style ="position: absolute;top:'+ data[i].top+'%;left:'+data[i].left+'%;transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);background-color: #555;color: white;font-size: 8px;border: none;cursor: pointer;border-radius: 5px;text-align: center; width: 20px; height: 20px;" value="'+data[i].label+'"/>';
                                      console.log(r)
                                      $(".container.khung").append(r);
                                  }
                                  // })// remove GIF loader 
                              })
                                     // add a message to say it's a success
                          }else{
                              alert('file not uploaded');
                          }
                      },
                  });
              });
          });

        </script>
        </div>
    </div>
  </div>
  
    <?php include '../footer-page.php';?>