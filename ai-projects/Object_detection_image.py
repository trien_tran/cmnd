#!/usr/bin/env python 
# -*- coding:utf-8 -*- 
######## Image Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/15/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier uses it to perform object detection on an image.
# It draws boxes and scores around the objects of interest in the image.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2
import argparse
import os.path
import time
import numpy as np
import tensorflow as tf
import sys
import urllib.request
import requests
import json
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
# Import utilites
from utils import convert_to_mp4
from utils import label_map_util_sach
from utils import visualization_utils_sach as vis_util_sach
from utils import label_map_util_thuoc
from utils import visualization_utils_thuoc as vis_util_thuoc
from flask import Flask, jsonify, request
from flask_cors import CORS
import pandas as pd
from keras.preprocessing import text, sequence
from keras.models import load_model
from keras.losses import binary_crossentropy
# from keras import backend as K
import pytesseract
import re
#detect toxic english-------------------
# def preprocess(data):
#         '''
#         Credit goes to https://www.kaggle.com/gpreda/jigsaw-fast-compact-solution
#         '''
#         punct = "/-'?!.,#$%\'()*+-/:;<=>@[\\]^_`{|}~`" + '""“”’' + '∞θ÷α•à−β∅³π‘₹´°£€\×™√²—–&'
#         def clean_special_chars(text, punct):
#             for p in punct:
#                 text = text.replace(p, ' ')
#             return text

#         data = data.astype(str).apply(lambda x: clean_special_chars(x, punct))
#         return data

# def fit_on_text_data_commenteng():
#     tokenizer = text.Tokenizer()
#     train = pd.read_csv('train.csv')
#     test = pd.read_csv('test.csv') 
#     x_train = preprocess(train['comment_text'])
#     x_testt = preprocess(test['comment_text'])       
#     tokenizer.fit_on_texts(list(x_train) + list(x_testt))
#     # model = load_model("model.h5",custom_objects={'custom_loss':custom_loss})
#     return tokenizer
# tokenizer = fit_on_text_data_commenteng()
 

#end detect toxic english-------------------


#detect toxic vietnames-------------------

def preprocess_commnet_vie(text):
    text = re.sub('[\n!,.?@#]', '', text)
    text = text.lower()
    list_happen = ["😊","❤️","😁","😄","😆","😍","🤣","😂","🤩","😚","😋",'😜',"😝","🤗",":)",":}","^^",";)",
    "👌","=))","😅","👍","👍🏻","💕","❤","👏","💟","<3",":D",":P","^_^","😉","✌️"]
    list_sad = ["😡","🤔","🤨","😐","😏","😒","😶","🙄","😌","😔","🤕","🤒","👿","🤬","😤",'😫',"😩","😭",":(","😈","-_-","👎"]
    for happen in list_happen:          
        text = text.replace(happen, "vui")
    for sad in list_sad:          
        text = text.replace(sad, "tệ")
    # text = ViTokenizer.tokenize(text)
    return text


def fit_on_text_data_commentvie():
    train = pd.read_csv('data/train.txt',sep='\t', header=None)
    test = pd.read_csv('data/test.txt',sep='\t', header=None)
    xtrain1 = train
    xtrain2 = test
    xtrain = []
    ytrain = []
    xtest = []
    for index,row in xtrain1.iterrows():
        if row[0] == "0" or  row[0] == "1": 
            preprocess_commnet_vie(row[0])
            ytrain.append(row[0])
        elif row[0][0:6] != "train_":
            xtrain.append(preprocess_commnet_vie(row[0]))


    for index,row in xtrain2.iterrows():
        # print(row[0])
        if row[0][0:5] != "test_":
            xtest.append(preprocess_commnet_vie(row[0]))
    tokenizer = text.Tokenizer()
    tokenizer.fit_on_texts(xtrain + xtest)
    # print(tokenizer.word_index)
    return tokenizer

tokenizer_vie = fit_on_text_data_commentvie()
from tensorflow import keras
# session = tf.compat.v1.Session()
# print("dssd",keras.__version__)
# print("dssd",tf.__version__)
# keras.backend.set_session(session)
# model = keras.models.load_model("model_toxic_vie.h5")
# model_vie = load_model("model_toxic_vie.h5")
# print("done")
#end detect toxic vietnames-------------------

app = Flask(__name__)
# app.config['SERVER_NAME'] = '124.0.0.1:5555'
# app.config.from_pyfile('config.cfg')
CORS(app)
# def fit_on_text_data():
#     print("dsdsd")


# @app.route('/image/<path:link>',methods = ['GET'])
# # print(link)
# def returnSach(link):
#     print("link",link)
#     MODEL_NAME = 'trained-inference-graphs/output_inference_graph_v1.pb'
#     # IMAGE_NAME = '/home/ttest_images/image8.jpg'

#     # url = requests.get('http://127.0.0.1:8082/image').json()["url"]         #lấy ra url
#     # print (url)
#     # req = urllib.request.urlopen(link)
#     # arr = np.asarray(bytearray(req.read()), dtype=np.uint8)                 #dua hinh anh ve [255 216 255 ... 215 255 217] de doc
#     # Grab path to current working directory
#     CWD_PATH = os.getcwd()
#     PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

#     # Path to label map file
#     PATH_TO_LABELS = os.path.join(CWD_PATH,'training','label_map.pbtxt')  #khong can CWD_PATH cung dc :))

#     # Path to image
#     # PATH_TO_IMAGE = os.path.join(CWD_PATH,IMAGE_NAME)
#     # PATH_TO_IMAGE = arr
#     # Number of classes the object detector can identify
#     NUM_CLASSES = 10                                                     # chú ý thay đổi

#     # đọc file label_map.pbtxt
#     label_map = label_map_util_sach.load_labelmap(PATH_TO_LABELS)
#     # chuyển object label_map trên thành 1 mảng các dict [{'id': 1, 'name': u'sachh'}, {'id': 2, 'name': u'coc'}]
#     categories = label_map_util_sach.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
#     # chuyển mảng categories trên thành 1 list kiểu dict {1: {'id': 1, 'name': u'sachh'}, 2: {'id': 2, 'name': u'coc'}}
#     category_index = label_map_util_sach.create_category_index(categories)
#     # Load the Tensorflow model into memory.(Tải mô hình Tensorflow vào bộ nhớ)
#     detection_graph = tf.Graph()
#     with detection_graph.as_default(): #tạo 1 graph mặc định
#         od_graph_def = tf.GraphDef()
#         with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
#             serialized_graph = fid.read()   #Trả về nội dung của tệp dưới dạng chuỗi ký tư mã hóa.
#             od_graph_def.ParseFromString(serialized_graph)  # paser chuỗi trên về string 
#             tf.import_graph_def(od_graph_def, name='')       # nhúng chuỗi trên vào graph

#         sess = tf.Session(graph=detection_graph)
#     # Define input and output tensors (i.e. data) for the object detection classifier (Xác định các tensor đầu vào và đầu ra (tức là dữ liệu) cho trình phân loại phát hiện đối tượng)

#     # Input tensor is the image
#     image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
#     # Output tensors are the detection boxes, scores, and classes  (Các thang đo đầu ra là các hộp phát hiện, điểm số và các lớp)
#     # Each box represents a part of the image where a particular object was detected (Mỗi hộp đại diện cho một phần của hình ảnh nơi phát hiện một đối tượng cụ thể)
#     detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

#     # Each score represents level of confidence for each of the objects. (Mỗi điểm thể hiện mức độ tự tin cho từng đối tượng)
#     # The score is shown on the result image, together with the class label.  (Điểm được hiển thị trên hình ảnh kết quả, cùng với nhãn lớp.)
#     detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
#     detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

#     # Number of objects detected (Số lượng đối tượng được phát hiện)
#     num_detections = detection_graph.get_tensor_by_name('num_detections:0')

#     # Load image using OpenCV and
#     # expand image dimensions to have shape: [1, None, None, 3]
#     # i.e. a single-column array, where each item in the column has the pixel RGB value


#     # Tải hình ảnh bằng OpenCV và
#     # mở rộng kích thước hình ảnh để có hình dạng: [1, Không có, Không có, 3]
#     # tức là một mảng một cột, trong đó mỗi mục trong cột có giá trị RGB pixel

#     image = cv2.imread(link)
#     # image = cv2.imdecode(PATH_TO_IMAGE, -1)
#     image_expanded = np.expand_dims(image, axis=0)  # mở rộng mảng theo trục x
#     """
#     x = np.array([1,2])
#     y = np.expand_dims(x, axis=0)
#     print(y)
#     array([[1, 2]])
#     """

#     # Perform the actual detection by running the model with the image as input(Thực hiện phát hiện thực tế bằng cách chạy mô hình với hình ảnh làm đầu vào)
#     (boxes, scores, classes, num) = sess.run(
#         [detection_boxes, detection_scores, detection_classes, num_detections],
#         feed_dict={image_tensor: image_expanded})

#     # Draw the results of the detection (aka 'visulaize the results') (Vẽ kết quả phát hiện (còn gọi là 'hiển thị kết quả'))

#     place = vis_util_sach.visualize_boxes_and_labels_on_image_array(
#         image,
#         np.squeeze(boxes),
#         np.squeeze(classes).astype(np.int32),
#         np.squeeze(scores),
#         category_index,
#         use_normalized_coordinates=True,
#         line_thickness=8,
#         min_score_thresh=0.80)
#     # places = json.dumps(place)
#     # cv2.imshow('Object detector', image)
#     return jsonify(place)


# @app.route('/thuoc/<path:link>',methods = ['GET']) 
# def returnThuoc(link):
#     def deskew(im, max_skew=10):
#         # print("dsa",im.shape)
#         height, width,z = im.shape

#         # Create a grayscale image and denoise it
#         im_gs = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
#         im_gs = cv2.fastNlMeansDenoising(im_gs, h=3)

#         # Create an inverted B&W copy using Otsu (automatic) thresholding
#         im_bw = cv2.threshold(im_gs, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

#         # Detect lines in this image. Parameters here mostly arrived at by trial and error.
#         lines = cv2.HoughLinesP(
#             # im_bw, 1, np.pi / 180, 200, minLineLength=width / 12, maxLineGap=width / 150
#             im_bw, 1, np.pi / 180, 100, minLineLength=100, maxLineGap=50
#         )
#         # print("line",lines)
#         if lines is None:
#             return "false"

#         # Collect the angles of these lines (in radians)
#         angles = []
#         for line in lines:
#             x1, y1, x2, y2 = line[0]
#             angles.append(np.arctan2(y2 - y1, x2 - x1))

#         # If the majority of our lines are vertical, this is probably a landscape image
#         landscape = np.sum([abs(angle) > np.pi / 4 for angle in angles]) > len(angles) / 2

#         # Filter the angles to remove outliers based on max_skew
#         if landscape:
#             angles = [
#                 angle
#                 for angle in angles
#                 if np.deg2rad(90 - max_skew) < abs(angle) < np.deg2rad(90 + max_skew)
#             ]
#         else:
#             angles = [angle for angle in angles if abs(angle) < np.deg2rad(max_skew)]

#         if len(angles) < 5:
#             # Insufficient data to deskew
#             return im

#         # Average the angles to a degree offset
#         angle_deg = np.rad2deg(np.median(angles))

#         # If this is landscape image, rotate the entire canvas appropriately
#         if landscape:
#             if angle_deg < 0:
#                 im = cv2.rotate(im, cv2.ROTATE_90_CLOCKWISE)
#                 angle_deg += 90
#             elif angle_deg > 0:
#                 im = cv2.rotate(im, cv2.ROTATE_90_COUNTERCLOCKWISE)
#                 angle_deg -= 90

#         # Rotate the image by the residual offset
#         M = cv2.getRotationMatrix2D((width / 2, height / 2), angle_deg, 1)
#         im = cv2.warpAffine(im, M, (width, height), borderMode=cv2.BORDER_REPLICATE)
#         return im
#     listLink = link.split("-",1)
#     MODEL_NAME = ""
#     if listLink[0] == "bvDaKhoaSG":
#         MODEL_NAME = 'model/bvDaKhoaSaiGon/output_inference_graph_v18947.pb'

#     PATH_TO_CKPT = os.path.join(MODEL_NAME,'frozen_inference_graph.pb')

#     # Path to label map file
#     PATH_TO_LABELS = os.path.join('label_map_thuoc.pbtxt')  #khong can CWD_PATH cung dc :))

#     NUM_CLASSES = 3

#     label_map = label_map_util_thuoc.load_labelmap(PATH_TO_LABELS)
#     categories = label_map_util_thuoc.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
#     category_index = label_map_util_thuoc.create_category_index(categories)

#     # Load the Tensorflow model into memory.(Tải mô hình Tensorflow vào bộ nhớ)
#     detection_graph = tf.Graph()
#     with detection_graph.as_default():
#         od_graph_def = tf.GraphDef()
#         with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
#             serialized_graph = fid.read()
#             od_graph_def.ParseFromString(serialized_graph)
#             tf.import_graph_def(od_graph_def, name='')

#         sess = tf.Session(graph=detection_graph)

#     # Define input and output tensors (i.e. data) for the object detection classifier (Xác định các tensor đầu vào và đầu ra (tức là dữ liệu) cho trình phân loại phát hiện đối tượng)

#     # Input tensor is the image
#     image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

#     # Output tensors are the detection boxes, scores, and classes  (Các thang đo đầu ra là các hộp phát hiện, điểm số và các lớp)
#     # Each box represents a part of the image where a particular object was detected (Mỗi hộp đại diện cho một phần của hình ảnh nơi phát hiện một đối tượng cụ thể)
#     detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

#     # Each score represents level of confidence for each of the objects. (Mỗi điểm thể hiện mức độ tự tin cho từng đối tượng)
#     # The score is shown on the result image, together with the class label.  (Điểm được hiển thị trên hình ảnh kết quả, cùng với nhãn lớp.)
#     detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
#     detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

#     # Number of objects detected (Số lượng đối tượng được phát hiện)
#     num_detections = detection_graph.get_tensor_by_name('num_detections:0')

#     # Load image using OpenCV and
#     # expand image dimensions to have shape: [1, None, None, 3]
#     # i.e. a single-column array, where each item in the column has the pixel RGB value


#     # Tải hình ảnh bằng OpenCV và
#     # mở rộng kích thước hình ảnh để có hình dạng: [1, Không có, Không có, 3]
#     # tức là một mảng một cột, trong đó mỗi mục trong cột có giá trị RGB pixel

#     image = cv2.imread(listLink[1])
#     # image = cv2.imdecode(PATH_TO_IMAGE, -1)
#     orig = image.copy()
#     image_expanded = np.expand_dims(image, axis=0)
#     # print("image_expanded",image_expanded)
#     # Perform the actual detection by running the model with the image as input(Thực hiện phát hiện thực tế bằng cách chạy mô hình với hình ảnh làm đầu vào)
#     (boxes, scores, classes, num) = sess.run(
#         [detection_boxes, detection_scores, detection_classes, num_detections],
#         feed_dict={image_tensor: image_expanded})

#     # Draw the results of the detection (aka 'visulaize the results') (Vẽ kết quả phát hiện (còn gọi là 'hiển thị kết quả'))
#     # print("np.squeeze(boxes)",np.squeeze(boxes))
#     # print("np.squeeze(classes).astype(np.int32)",np.squeeze(classes).astype(np.int32))
#     # print("np.squeeze(scores)",np.squeeze(scores))
#     # print("np.squeeze(boxes)",np.squeeze(boxes))

#     image,box_crop,box_to_display_str_map = vis_util_thuoc.visualize_boxes_and_labels_on_image_array(
#         image,
#         np.squeeze(boxes),
#         np.squeeze(classes).astype(np.int32),
#         np.squeeze(scores),
#         category_index,
#         use_normalized_coordinates=True,
#         line_thickness=8,
#         min_score_thresh=0.80)
#     # cv2.imwrite("C:/Users/anlan/OneDrive/Desktop/out.jpg", image)
#     # cv2.waitKey(0)
#     # cắt ảnh theo tọa độ và dùng tesseract để detect text
#     height,width,z = orig.shape   #lấy ra chiều cáo, rộng,kênh màu của ảnh
#     listThuoc = {}
#     # listThuocAvage = []
#     listSL = {}
#     # listSLAvage = []
#     listLieu = {}
#     # listLieuAvage = []
#     for box,color in box_crop:
#         ymin,xmin,ymax,xmax = box
#         label = box_to_display_str_map[box]
#         # print("label",label)
#         # print("l",label[0][0])
#         if label[0][0] == "t":
#             # listThuocAvage.append((ymin+ymax)/2)
#             listThuoc[(ymin+ymax)/2] = box  # luu thuoc theo dang 0.5020196735858917: (0.4893898367881775, 0.08889280259609222, 0.514649510383606, 0.4618086814880371)
#         elif label[0][0] == "l":
#             # listLieuAvage.append((ymin+ymax)/2)
#             listLieu[(ymin+ymax)/2] = box
#         elif label[0][0] == "s":
#             # listSLAvage.append((ymin+ymax)/2)
#             listSL[(ymin+ymax)/2] = box
#     # print("listThuoc = []",listThuoc)
#     # print("listSL = []",listSL)
#     # print("listLieu = []",listLieu)

#     listThuoc1 = sorted(listThuoc)  #sắp xếp tăng dần theo key của listThuoc
#     listSL1 = sorted(listSL)
#     listLieu1 = sorted(listLieu)
#     print("listThuoc1",listThuoc1)# ------
#     print("listSL1",listSL1)# ------
#     print("listLieu1",listLieu1)# ------

#     listBox = []

#     for i in range(len(listThuoc1)):
#     # print("ii",i)
#         thuoc = []
#         if listThuoc1[i] > 0.35:
#             thuoc.append(listThuoc[listThuoc1[i]])
#             ymint,xmint,ymaxt,xmaxt = listThuoc[listThuoc1[i]]
#             # number = 0
#             # numberr = 0
#             # listL = []
#             listl = {}
#             listsl = {}
#             for j in range(len(listLieu1)):
#                 yminl,xminl,ymaxl,xmaxl = listLieu[listLieu1[j]]
#                 if 0 < (listLieu1[j] - listThuoc1[i]) and abs(ymaxt - yminl) < 0.01  : #lieu phai o sau thuoc
#                     listl[(listLieu1[j] - listThuoc1[i])] = j #lưu khoảng cách listLieu1[j] và listThuoc1[i] : j
#             if len(listl) > 0:
#                 result = sorted(listl)  #sắp xếp theo khoảng cách gần nhất
#                 thuoc.append(listLieu[listLieu1[listl[result[0]]]])
#                 del listLieu1[listl[result[0]]]
#             elif len(listl) == 0:
#                 thuoc.append("")
#             for x in range(len(listSL1)):
#                 yminsl,xminsl,ymaxsl,xmaxsl = listSL[listSL1[x]]
#                 # if abs((listSL1[x] - listThuoc1[i])) < 0.03:
#                 # print("yminsl - ymaxt",ymaxt - yminsl)
#                 # if 0 < (ymaxt - yminsl) < 0.055:
#                 if 0 < (ymaxt - yminsl) and abs(ymint - yminsl) < 0.03:
#                     print("da qua")
#                     listsl[abs((listSL1[x] - listThuoc1[i]))] = x
#             if len(listsl) > 0:
#                 resultt = sorted(listsl)
#                 thuoc.append(listSL[listSL1[listsl[resultt[0]]]])
#                 del listSL1[listsl[resultt[0]]]
#             elif len(listsl) == 0:
#                 thuoc.append("")
#             listBox.append(thuoc)
#     print("listBox",listBox)# ------
#     # config = ('-l eng+vie --oem 1 --psm 11')
#     result = []
#     for i in range(len(listBox)):
#         dictThuoc = {}
#         for j in range(len(listBox[i])):
#             if listBox[i][j] != "":
#                 ymin,xmin,ymax,xmax = listBox[i][j]
#                 xmin_crop = int(xmin*width)
#                 # print("xmin_crop",xmin_crop)
#                 xmax_crop = int(xmax*width)
#                 # print("xmax_crop",xmax_crop)
#                 ymin_crop = int(ymin*height)
#                 # print("ymin_crop",ymin_crop)
#                 ymax_crop = int(ymax*height)
#                 # print("ymax_crop",ymax_crop)
#                 # roi = orig[ymin_crop:ymax_crop, xmin_crop:xmax_crop]
#                 roi1 = orig[ymin_crop:ymax_crop, xmin_crop:xmax_crop]
#                 # print("roi",roi1.shape)
#                 # cv2.imshow("sd",roi)
#                 roi2 = deskew(roi1,max_skew=10)
#                 if roi2 == "false":
#                     roi = roi1
#                 else: roi = roi2
#                 if j == 0:
#                     # crop_name = 'C:/Users/anlan/OneDrive/Desktop/resultImg/resultImg/thuoc'+ str(i) +'.jpg'
#                     # cv2.imwrite(crop_name, roi)
#                     config = ('-l eng --oem 1 --psm 11')
#                     text = pytesseract.image_to_string(roi, config=config)
#                     text = re.sub(r"\n", ' ', text)  #xóa \n
#                     dictThuoc ["thuoc"] = text
#                 if j == 1:
#                     # crop_name = 'C:/Users/anlan/OneDrive/Desktop/resultImg/resultImg/lieu'+ str(i) +'.jpg'
#                     # cv2.imwrite(crop_name, roi)
#                     config = ('-l vie --oem 1 --psm 11')
#                     text = pytesseract.image_to_string(roi, config=config)
#                     text = re.sub(r"\n", ' ', text)
#                     dictThuoc ["lieu"] = text
#                 if j == 2:
#                     # crop_name = 'C:/Users/anlan/OneDrive/Desktop/resultImg/resultImg/sl'+ str(i) +'.jpg'
#                     # cv2.imwrite(crop_name, roi)
#                     config = ('-l vie --oem 1 --psm 11')
#                     text = pytesseract.image_to_string(roi, config=config)
#                     text = re.sub(r"\n", ' ', text)
#                     dictThuoc ["sl"] = text
#         result.append(dictThuoc)
#     print("result",result)# ------
#     cv2.destroyAllWindows()
#     return jsonify(result)
    
# @app.route('/comment/<path:link>',methods = ['GET'])
# def returnComment(link):
#     import numpy as np
#     import pandas as pd
#     from keras.models import Model
#     # from keras.layers import Input, Dense, Embedding, SpatialDropout1D, Dropout, add, concatenate
#     # from keras.layers import LSTM, Bidirectional, GlobalMaxPooling1D, GlobalAveragePooling1D
#     from keras.preprocessing import text, sequence
#     # from keras.callbacks import LearningRateScheduler
#     from keras.losses import binary_crossentropy
#     from keras import backend as K
#     from keras.models import load_model
#     # from keras.losses import mean_squared_abs_error



#     def custom_loss(y_true, y_pred):
#         return binary_crossentropy(K.reshape(y_true[:,0],(-1,1)), y_pred) * y_true[:,1]

#     def preprocess(data):
#         '''
#         Credit goes to https://www.kaggle.com/gpreda/jigsaw-fast-compact-solution
#         '''
#         punct = "/-'?!.,#$%\'()*+-/:;<=>@[\\]^_`{|}~`" + '""“”’' + '∞θ÷α•à−β∅³π‘₹´°£€\×™√²—–&'
#         def clean_special_chars(text, punct):
#             for p in punct:
#                 text = text.replace(p, ' ')
#             return text

#         data = data.astype(str).apply(lambda x: clean_special_chars(x, punct))
#         return data

#     MAX_LEN = 220
#     train = pd.read_csv('train.csv')
#     test = pd.read_csv('test.csv')
#     # print("test",test)
#     x_train = preprocess(train['comment_text'])
#     x_testt = preprocess(test['comment_text'])
#     # link = "fuck you"
#     x_test = pd.DataFrame([link]) #chuyen str thành  DataFrame
#     x_test = x_test.iloc[0]
#     x_test = preprocess(x_test)
#     tokenizer = text.Tokenizer()
#     # tokenizer.fit_on_texts(list(x_test))
#     tokenizer.fit_on_texts(list(x_train) + list(x_testt))
#     # print("tokenizer",tokenizer)
#     x_test = tokenizer.texts_to_sequences(x_test)
#     x_test = sequence.pad_sequences(x_test, maxlen=MAX_LEN) 

#     # print("x_test",x_test.shape)
#     model = load_model("model.h5",custom_objects={'custom_loss':custom_loss})

#     predict = model.predict(x_test, batch_size=2048)[0].flatten()
#     print("predict",predict[0])
#     if predict[0] > 0.5:
#         return jsonify(["Toxicity"])
#     else:
#         return jsonify(["No toxicity"])


# @app.route('/commentvie/<path:link>',methods = ['GET'])
# def returncommentvie(link):
#     # import numpy as np
#     # import pandas as pd
#     # from keras.models import Model
#     from keras.preprocessing import text, sequence
#     # from keras.losses import binary_crossentropy
#     from keras import backend as K
#     from keras.models import load_model
#     import re
#     import time
#     import tensorflow as tf
#     before = time.time()
#     # from pyvi import ViTokenizer
#     MAX_LEN = 220
#     K.clear_session ()

#     # def preprocess_commnet_vie(text):
#     #     text = re.sub('[\n!,.?@#]', '', text)
#     #     text = text.lower()
#     #     list_happen = ["😊","❤️","😁","😄","😆","😍","🤣","😂","🤩","😚","😋",'😜',"😝","🤗",":)",":}","^^",";)",
#     #     "👌","=))","😅","👍","👍🏻","💕","❤","👏","💟","<3",":D",":P","^_^","😉","✌️"]
#     #     list_sad = ["😡","🤔","🤨","😐","😏","😒","😶","🙄","😌","😔","🤕","🤒","👿","🤬","😤",'😫',"😩","😭",":(","😈","-_-","👎"]
#     #     for happen in list_happen:          
#     #         text = text.replace(happen, "vui")
#     #     for sad in list_sad:          
#     #         text = text.replace(sad, "tệ")
#     #     # text = ViTokenizer.tokenize(text)
#     #     return text

#     # train = pd.read_csv('data/train.txt',sep='\t', header=None)
#     # test = pd.read_csv('data/test.txt',sep='\t', header=None)
#     # xtrain1 = train
#     # xtrain2 = test
#     # xtrain = []
#     # ytrain = []
#     # xtest = []
#     # for index,row in xtrain1.iterrows():
#     #     if row[0] == "0" or  row[0] == "1": 
#     #         preprocess_commnet_vie(row[0])
#     #         ytrain.append(row[0])
#     #     elif row[0][0:6] != "train_":
#     #         xtrain.append(preprocess_commnet_vie(row[0]))


#     # for index,row in xtrain2.iterrows():
#     #     # print(row[0])
#     #     if row[0][0:5] != "test_":
#     #         xtest.append(preprocess_commnet_vie(row[0]))
#     # tokenizer_vie = text.Tokenizer()
#     # tokenizer_vie.fit_on_texts(xtrain + xtest) #tạo ra 1 từ điển#tạo ra 1 từ điển
#     x_test = [preprocess_commnet_vie(link)]
#     x_test = tokenizer_vie.texts_to_sequences(x_test)
#     x_test = sequence.pad_sequences(x_test, maxlen=MAX_LEN)
#     # print("x_test",x_test)

#     # model = load_model("model_toxic_vie.h5")
#     # predict = model.predict(np.array(x_test))
#     with session.as_default():
#         with session.graph.as_default():
#             predict = model.predict(np.array(x_test))
#     print(predict)
#     print(predict[0][0])
#     K.clear_session ()
#     timer = time.time() - before
#     print("thời gian chạy",timer)
#     if predict[0][0] > 0.5:
#         return jsonify(["không hài lòng"])
#     else:
#         return jsonify(["hài lòng"])
    

# @app.route('/violence/<path:link>',methods = ['GET'])
# def returnViolence(link):
#     sys.path.append("..")
#     before = time.time()
#     # Import utilites
#     from utils import label_map_util_violence
#     from utils import visualization_utils_violence as vis_util_violence

#     # Name of the directory containing the object detection module we're using
#     # MODEL_NAME = 'inference_graph'
#     MODEL_NAME = 'output_inference_graph_v2.pb'
#     # MODEL_NAME = 'C:/Users/anlan/OneDrive/Desktop/output_inference_graph_v2.pb'
#     # VIDEO_NAME = 'C:/Users/anlan/OneDrive/Desktop/frame_violence/violence_nha_tu1.mp4'
#     # VIDEO_NAME = 'C:/Users/anlan/OneDrive/Desktop/frame_violence/violence_tram_xang.mp4'
#     # VIDEO_NAME = 'C:/Users/anlan/Downloads/Telegram Desktop/demo_nha_tu.mp4'
#     url = link.split("/",1)
#     print("url",url)

#     # Grab path to current working directory
#     CWD_PATH = os.getcwd()

#     # Path to frozen detection graph .pb file, which contains the model that is used
#     # for object detection.
#     PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

#     # Path to label map file
#     PATH_TO_LABELS = os.path.join('label_map.pbtxt')

#     # Path to video
#     PATH_TO_VIDEO = os.path.join(CWD_PATH,url[1])

#     # Number of classes the object detector can identify
#     NUM_CLASSES = 2

#     # Load the label map.
#     # Label maps map indices to category names, so that when our convolution
#     # network predicts `5`, we know that this corresponds to `king`.
#     # Here we use internal utility functions, but anything that returns a
#     # dictionary mapping integers to appropriate string labels would be fine
#     label_map = label_map_util_violence.load_labelmap(PATH_TO_LABELS)
#     categories = label_map_util_violence.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
#     category_index = label_map_util_violence.create_category_index(categories)


#     # class VideoSavor:
#     # 	def AppendFrame(self, image_):
#     # 		self.outputStream.write(image_)

#     # 	def __init__(self, targetFileName, videoCapture):
#     # 		width = int(500)
#     # 		height = int(500)
#     # 		frameRate = int( videoCapture.get(cv2.CAP_PROP_FPS) )
#     # 		codec = cv2.VideoWriter_fourcc(*'XVID')
#     # 		self.outputStream = cv2.VideoWriter(targetFileName + ".avi", codec, frameRate, (width, height) )



#     # Load the Tensorflow model into memory.
#     detection_graph = tf.Graph()
#     with detection_graph.as_default():
#         od_graph_def = tf.GraphDef()
#         with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
#             serialized_graph = fid.read()
#             od_graph_def.ParseFromString(serialized_graph)
#             tf.import_graph_def(od_graph_def, name='')

#         sess = tf.Session(graph=detection_graph)

#     # Define input and output tensors (i.e. data) for the object detection classifier

#     # Input tensor is the image
#     image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

#     # Output tensors are the detection boxes, scores, and classes
#     # Each box represents a part of the image where a particular object was detected
#     detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

#     # Each score represents level of confidence for each of the objects.
#     # The score is shown on the result image, together with the class label.
#     detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
#     detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

#     # Number of objects detected
#     num_detections = detection_graph.get_tensor_by_name('num_detections:0')
#     PATH_FILE_NAME_TO_SAVE_RESULT = "result/output8.avi"
#     # Open video file

#     video = cv2.VideoCapture(PATH_TO_VIDEO)
#     fourcc = cv2.VideoWriter_fourcc(*'XVID')
#     out = cv2.VideoWriter(PATH_FILE_NAME_TO_SAVE_RESULT,fourcc, 20.0,(int(video.get(3)), int(video.get(4))))  #(int(video.get(3)), int(video.get(4) chiều cao,rông của input = chiều cao rộng của output
#     # videoSavor = VideoSavor(PATH_FILE_NAME_TO_SAVE_RESULT + "_Result", video) # luu video
#     while(video.isOpened()):

#         # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
#         # i.e. a single-column array, where each item in the column has the pixel RGB value
#         ret, frame = video.read()
#         frame_expanded = np.expand_dims(frame, axis=0)
#         if frame_expanded[0] is None:
#             break;
#         # Perform the actual detection by running the model with the image as input
#         (boxes, scores, classes, num) = sess.run(
#             [detection_boxes, detection_scores, detection_classes, num_detections],
#             feed_dict={image_tensor: frame_expanded})
#         # print("np.squeeze(classes).astype(np.int32)",np.squeeze(classes).astype(np.int32))
#         # print("category_index",category_index)
#         # print([category_index.get(i) for i in classes[0]])
#         # print(scores)
#         # Draw the results of the detection (aka 'visulaize the results')
#         place = vis_util_violence.visualize_boxes_and_labels_on_image_array(
#             frame,
#             np.squeeze(boxes),
#             np.squeeze(classes).astype(np.int32),
#             np.squeeze(scores),
#             category_index,
#             use_normalized_coordinates=True,
#             line_thickness=8,
#             min_score_thresh=0.80)
#         out.write(frame)
#         # print("place",place)
#         # print("frame",frame)
#         # All the results have been drawn on the frame, so it's time to display it.
#         # cv2.imshow('Object detector', frame)
#         # videoSavor.AppendFrame(frame)
#         # Press 'q' to quit
#         # print("dasdadas")
#         # if cv2.waitKey(1) == ord('q'):
#             # break
#     # print("dadadadada")
#     # Clean up
#     video.release()
#     cv2.destroyAllWindows()
#     a = convert_to_mp4.convert_video('result/output8.avi','result/output8.mp4')
#     print(a)
#     if a == True:
#         timer = time.time() - before
#         print("thời gian chạy",timer)
#         return jsonify(["result/output8.mp4"])

# @app.route('/logo/<path:link>',methods = ['GET'])
# def returnLogo(link):
#     print("link",link)
#     sys.path.append("..")
#     before = time.time()
#     confThreshold = 0.5  #Confidence threshold
#     nmsThreshold = 0.4  #Non-maximum suppression threshold

#     inpWidth = 416  #608     #Width of network's input image
#     inpHeight = 416 #608     #Height of network's input image

#     # parser = argparse.ArgumentParser(description='Object Detection using YOLO in OPENCV')
#     # parser.add_argument('--image', help='Path to image file.')
#     # parser.add_argument('--video', help='Path to video file.')
#     # args = parser.parse_args()
            
#     # Load names of classes
#     classesFile = "detect_logo_yolo/classes.names";

#     classes = None
#     with open(classesFile, 'rt') as f:
#         classes = f.read().rstrip('\n').split('\n')

#     # Give the configuration and weight files for the model and load the network using them.

#     modelConfiguration = "detect_logo_yolo/yolov3-tiny.cfg";
#     modelWeights = "detect_logo_yolo/yolov3-tiny_logo_19000.weights";

#     net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
#     print("done")
#     net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
#     net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

#     # Get the names of the output layers
#     def getOutputsNames(net):
#         # Get the names of all the layers in the network
#         layersNames = net.getLayerNames()
#         # Get the names of the output layers, i.e. the layers with unconnected outputs
#         return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

#     # Draw the predicted bounding box
#     def drawPred(classId, conf, left, top, right, bottom):
#         # Draw a bounding box.
#         #    cv2.rectangle(frame, (left, top), (right, bottom), (255, 178, 50), 3)
#         cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 3)

#         label = '%.2f' % conf
            
#         # Get the label for the class name and its confidence
#         if classes:
#             assert(classId < len(classes))
#             label = '%s:%s' % (classes[classId], label)

#         #Display the label at the top of the bounding box
#         labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
#         top = max(top, labelSize[1])
#         cv2.rectangle(frame, (left, top - round(1.5*labelSize[1])), (left + round(1.5*labelSize[0]), top + baseLine), (0, 0, 255), cv2.FILLED)
#         #cv2.rectangle(frame, (left, top - round(1.5*labelSize[1])), (left + round(1.5*labelSize[0]), top + baseLine),    (255, 255, 255), cv2.FILLED)
#         cv2.putText(frame, label, (left, top), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,0), 2)

#     # Remove the bounding boxes with low confidence using non-maxima suppression
#     def postprocess(frame, outs):
#         frameHeight = frame.shape[0]
#         frameWidth = frame.shape[1]

#         classIds = []
#         confidences = []
#         boxes = []
#         # Scan through all the bounding boxes output from the network and keep only the
#         # ones with high confidence scores. Assign the box's class label as the class with the highest score.
#         classIds = []
#         confidences = []
#         boxes = []
#         for out in outs:
#             # print("out.shape : ", out.shape)
#             for detection in out:
#                 #if detection[4]>0.001:
#                 scores = detection[5:]
#                 classId = np.argmax(scores)
#                 #if scores[classId]>confThreshold:
#                 confidence = scores[classId]
#                 # if detection[4]>confThreshold:
#                     # print(detection[4], " - ", scores[classId], " - th : ", confThreshold)
#                     # print("detection")
#                 if confidence > confThreshold:
#                     center_x = int(detection[0] * frameWidth)
#                     center_y = int(detection[1] * frameHeight)
#                     width = int(detection[2] * frameWidth)
#                     height = int(detection[3] * frameHeight)
#                     left = int(center_x - width / 2)
#                     top = int(center_y - height / 2)
#                     classIds.append(classId)
#                     confidences.append(float(confidence))
#                     boxes.append([left, top, width, height])

#         # Perform non maximum suppression to eliminate redundant overlapping boxes with
#         # lower confidences.
#         indices = cv2.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
#         for i in indices:
#             i = i[0]
#             box = boxes[i]
#             left = box[0]
#             top = box[1]
#             width = box[2]
#             height = box[3]
#             drawPred(classIds[i], confidences[i], left, top, left + width, top + height)

#     # Process inputs
#     # winName = 'Deep learning object detection in OpenCV'
#     # cv2.namedWindow(winName, cv2.WINDOW_NORMAL)

#     outputFile = "result/yolo_out_py.avi"
#     cap = cv2.VideoCapture(link)
#     fourcc = cv2.VideoWriter_fourcc(*'XVID')

#     # Get the video writer initialized to save the output video
#     vid_writer = cv2.VideoWriter(outputFile,fourcc, 30, (round(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),round(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

#     while cv2.waitKey(1) < 0:
        
#         # get frame from the video
#         hasFrame, frame = cap.read()
        
#         # Stop the program if reached end of video
#         if not hasFrame:
#             # print("Done processing !!!")
#             # print("Output file is stored as ", outputFile)
#             cv2.waitKey(3000)
#             break

#         # Create a 4D blob from a frame.
#         blob = cv2.dnn.blobFromImage(frame, 1/255, (inpWidth, inpHeight), [0,0,0], 1, crop=False)

#         # Sets the input to the network
#         net.setInput(blob)

#         # Runs the forward pass to get output of the output layers
#         outs = net.forward(getOutputsNames(net))

#         # Remove the bounding boxes with low confidence
#         postprocess(frame, outs)

#         # Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
#         t, _ = net.getPerfProfile()
#         label = 'Inference time: %.2f ms' % (t * 1000.0 / cv2.getTickFrequency())
#         #cv2.putText(frame, label, (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

#         # Write the frame with the detection boxes
#         vid_writer.write(frame.astype(np.uint8))
#         # cv2.imshow(winName, frame)
#     cv2.destroyAllWindows()
#     a = convert_to_mp4.convert_video('result/yolo_out_py.avi','result/yolo_out_py.mp4')
#     print(a)
#     if a == True:
#         timer = time.time() - before
#         print("thời gian chạy",timer)
#         return jsonify(["result/yolo_out_py.mp4"])

@app.route('/cmnd/<path:link>',methods = ['GET']) 
def returnCmnd(link):

   
    import os
    import sys
    import json
    import datetime
    import numpy as np
    import skimage.draw
    from matplotlib import pyplot as plt
    import cv2
    from transform import four_point_transform
    import imutils
    from skimage.filters import threshold_local
    import pickle
    from config import Config
    # import utils_mask
    import model as modellib
    import tensorflow as tf
    import pytesseract
    import re
    from utils_cmnd import label_map_util
    from utils_cmnd import visualization_utils as vis_util
    print('xx',link)
    class BalloonConfig(Config):
        """Configuration for training on the toy  dataset.
        Derives from the base Config class and overrides some values.
        # Đặt cho cấu hình một tên dễ nhận biết 
        NAME = "ship" 
        # Chúng tôi sử dụng GPU có 12GB bộ nhớ, có thể phù hợp với hai hình ảnh. 
            # Điều chỉnh xuống nếu bạn sử dụng GPU nhỏ hơn. 
            IMAGES_PER_GPU = 1 
        # Số lớp (bao gồm cả nền) 
            NUM_CLASSES = 1 + 1 # Background + tàu 
        # Số bước đào tạo mỗi epoch
            STEPS_PER_EPOCH = 500 
        # Bỏ qua nhận diện với <95% độ tin cậy 
            DETECTION_MIN_CONFIDENCE = 0,95 
        # Non-tối đa ức chế ngưỡng để phát hiện
            DETMENT_NMS_THRESHOLD = 
            0,0 
        Thay đổi kích thước đầu vào
        IMAGE_MIN_DIM = 768 
        IMAGE_MAX_DIM = 768

        """
        # Give the configuration a recognizable name
        NAME = "balloon"

        # We use a GPU with 12GB memory, which can fit two images.
        # Adjust down if you use a smaller GPU.
        IMAGES_PER_GPU = 2

        # Number of classes (including background)
        NUM_CLASSES = 1 + 1  # Background + baloon

        # Number of training steps per epoch
        STEPS_PER_EPOCH = 500

        # Skip detections with < 90% confidence
        DETECTION_MIN_CONFIDENCE = 0.9

    class InferenceConfig(BalloonConfig):
        # Set batch size to 1 since we'll be running inference on
        # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

    
    config = InferenceConfig()
    config.display()

    weights_path = "detect_cmt/mask_rcnn_balloon_0021.h5"
    model = modellib.MaskRCNN(mode="inference", config=config,model_dir='mask_rcnn_coco.hy')
    model.load_weights(weights_path, by_name=True)
    def color_splash(image, mask):  #tạo ra hình ảnh chứa mask
        """Apply color splash effect.
        image: RGB image [height, width, 3]
        mask: instance segmentation mask [height, width, instance count]

        Returns result image.
        """
        # Make a grayscale copy of the image. The grayscale copy still
        # has 3 RGB channels, though.
        gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
        # print("gray",gray.shape)
        # skimage.io.imshow(image)
        # plt.show()
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        # print("mask",mask.shape)
        # plt.imshow(mask.reshape(720,492))
        # plt.show()
        # print("mask",mask.shape)
        # Copy color pixels from the original color image where mask is set
        if mask.shape[0] > 0:
            splash = np.where(mask, image, gray).astype(np.uint8)
            splashh = np.where(mask, image, gray)     
            # print("splashh",splashh)
        else:
            splash = gray
        return splash    
    print('xx',link)
    image = cv2.imread('/home/trientt/Desktop/trien.jpg')
    print('image',image)
    r = model.detect([image], verbose=1)[0]
    splash = color_splash(image, r['masks'])    
    reshape_mask = (np.sum(r['masks'], -1, keepdims=True) >= 1)  
    reshape_mask = reshape_mask.reshape(reshape_mask.shape[0], reshape_mask.shape[1])
    reshape_mask.dtype='uint8' 
    x, y, w, h = cv2.boundingRect(reshape_mask.copy())
    contours,_= cv2.findContours(reshape_mask.copy(), 1, 1)
    rect = cv2.minAreaRect(contours[0])
    (x,y),(w,h), reshape_mask = rect
    box = cv2.boxPoints(rect)
    box = np.int0(box) #turn into ints convert to int    
    rect2 = cv2.drawContours(image.copy(),[box],0,(0,0,255),3)
    warped = four_point_transform(image.copy(), box)
    img = cv2.cvtColor(warped.copy(), cv2.COLOR_BGR2GRAY)
    img = cv2.resize(img,(28,28))
    img = img/255.0
    img = img.reshape(1,28*28)
    with open('detect_cmt/cmt.pkl', 'rb') as f:
        clf = pickle.load(f)
        results=clf.predict(img)
        # print("results",results)
        if results[0] == 1:
            # print("qua")
            warped = imutils.rotate_bound(warped,180)   #'''nếu ngược xoay 180'''

    MODEL_NAME = 'detect_cmt/output_inference_graph_5678.pb'

    # Grab path to current working directory
    CWD_PATH = os.getcwd()
    # print(CWD_PATH)
    # Path to frozen detection graph .pb file, which contains the model that is used
    # for object detection.
    PATH_TO_CKPT = os.path.join(MODEL_NAME,'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join('detect_cmt/label_map.pbtxt')  #khong can CWD_PATH cung dc :))

    # Path to image
    # PATH_TO_IMAGE = os.path.join(CWD_PATH,IMAGE_NAME)
    # print(PATH_TO_IMAGE)
    # Number of classes the object detector can identify
    NUM_CLASSES = 4

    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes  (Các thang đo đầu ra là các hộp phát hiện, điểm số và các lớp)
    # Each box represents a part of the image where a particular object was detected (Mỗi hộp đại diện cho một phần của hình ảnh nơi phát hiện một đối tượng cụ thể)
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects. (Mỗi điểm thể hiện mức độ tự tin cho từng đối tượng)
    # The score is shown on the result image, together with the class label.  (Điểm được hiển thị trên hình ảnh kết quả, cùng với nhãn lớp.)
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected (Số lượng đối tượng được phát hiện)
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    image = warped
    orig = image.copy()
    # print("orig",orig.shape)
    image_expanded = np.expand_dims(image, axis=0)
    # print("image_expanded",image_expanded)
    # Perform the actual detection by running the model with the image as input(Thực hiện phát hiện thực tế bằng cách chạy mô hình với hình ảnh làm đầu vào)
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    # Draw the results of the detection (aka 'visulaize the results') (Vẽ kết quả phát hiện (còn gọi là 'hiển thị kết quả'))

    image,box_crop = vis_util.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=2,
        min_score_thresh=0.80)
    # print("box_crop",box_crop)
    # cv2.imshow("sds",image)
    # cv2.imwrite("C:/Users/anlan/OneDrive/Desktop/out.jpg", image)
    # cv2.waitKey(0)
    '''cắt ảnh theo tọa độ và dùng tesseract để detect text''' 
    height,width,z = orig.shape   #'''lấy ra chiều cao, rộng,kênh màu của ảnh'''


    ''' xóa các thường trú có score thấp'''
    list_index_thuong_tru = []
    list_score_thuong_tru = []
    for i in range(len(box_crop)):
        if box_crop[i][2] == 'thuongtru':
            list_index_thuong_tru.append(i)
            list_score_thuong_tru.append(box_crop[i][1])
    # print("list_index_thuong_tru",list_index_thuong_tru)
    # print("list_score_thuong_tru",list_score_thuong_tru)
    # print("max_index",max_index)
    if len(list_index_thuong_tru)> 1:
        max_index = np.argmax(np.array(list_score_thuong_tru))
        for i in range(len(list_index_thuong_tru)):
            if i == max_index:
                continue
            else:
                del box_crop[list_index_thuong_tru[i]]

    result = []
    for box in box_crop:
        ymin,xmin,ymax,xmax = box[0]       #lấy ra tọa độ detect box
        xmin_crop = int(xmin*width)
        # print("xmin_crop",xmin_crop)
        xmax_crop = int(xmax*width)
        # print("xmax_crop",xmax_crop)
        ymin_crop = int(ymin*height)
        # print("ymin_crop",ymin_crop)
        ymax_crop = int(ymax*height)
        # print("ymax_crop",ymax_crop)
        roi = orig[ymin_crop:ymax_crop, xmin_crop:xmax_crop]  #(ymin:ymax,xmin:xmax) ảnh sau khi crop
        config = ('-l vie --oem 1 --psm 11')
        print("config",pytesseract.get_languages())
        text = pytesseract.image_to_string(roi, config='eng')
        print(box[2] +":"+ text)
        result.append(box[2] +":"+ text)
    cv2.destroyAllWindows()
    return jsonify(result)




if __name__ == "__main__":
    app.run(debug=True,port=8080)

    # app.config['SERVER_NAME'] = "127.0.0.1:8080"
    # app.run()
#run http://127.0.0.1:8080/image/

