# import the necessary packages
import numpy as np
import cv2
import math
def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	list_canh_h = []
	d_box01 = math.sqrt(math.pow(pts[0][0]-pts[1][0],2)+math.pow(pts[0][1]-pts[1][1],2))  #khoảng cách tọa độ box[0] và  box[1]
	list_canh_h.append(d_box01)
	d_box12 = math.sqrt(math.pow(pts[1][0]-pts[2][0],2)+math.pow(pts[1][1]-pts[2][1],2))  #khoảng cách tọa độ box[1] và  box[2]
	list_canh_h.append(d_box12)

	chieu_dai = np.argmax(np.array(list_canh_h))
	chieu_rong = np.argmin(np.array(list_canh_h))
	if chieu_dai == 0:
		rect[0] = pts[0]
		rect[1] = pts[1]
		rect[3] = pts[3]
		rect[2] = pts[2]
	elif chieu_dai == 1:
		rect[0] = pts[1]
		rect[1] = pts[2]
		rect[2] = pts[3]
		rect[3] = pts[0]

	# rect[0] = pts[np.argmin(s)]  #trên cùng trái
	# rect[1] = pts[np.argmin(diff)] 	#trên cùng phải
	# rect[2] = pts[np.argmax(s)]		#dưới cùng phải
	# rect[3] = pts[np.argmax(diff)]	#dưới cùng trái
	return rect

def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	rect = order_points(pts)
	# rect = np.array([[ 16,413],[414,15],[676, 277],[278, 675]],np.float32)
	print("rect",rect)
	# rect = pts
	(tl, tr, br, bl) = rect
	print(tl, tr, br, bl)

	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	print("widthA",widthA)
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	print("widthB",widthB)
	maxWidth = max(int(widthA), int(widthB))
	print("maxWidth",maxWidth)


	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	print("heightA",heightA)
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	print("heightB",heightB)
	maxHeight = max(int(heightA), int(heightB))
	print("maxHeight",maxHeight)

	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	# if maxHeight > maxWidth:
	# 	a = maxWidth
	# 	maxWidth = maxHeight
	# 	maxHeight = a
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")

	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(rect, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

	# return the warped image
	return warped