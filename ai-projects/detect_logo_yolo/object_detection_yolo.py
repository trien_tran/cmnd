# This code is written at BigVision LLC. It is based on the OpenCV project. It is subject to the license terms in the LICENSE file found in this distribution and at http://opencv.org/license.html

# Usage example:  python3 object_detection_yolo.py --video=run.mp4
#                 python3 object_detection_yolo.py --image=bird.jpg

import cv2 as cv
import argparse
import sys
import numpy as np
import os.path
import time

before = time.time()
# Initialize the parameters
confThreshold = 0.5  #Confidence threshold
nmsThreshold = 0.4  #Non-maximum suppression threshold

inpWidth =    416  #608     #Width of network's input image
inpHeight =   416 #608     #Height of network's input image

parser = argparse.ArgumentParser(description='Object Detection using YOLO in OPENCV')
parser.add_argument('--image', help='Path to image file.')
parser.add_argument('--video', help='Path to video file.')
args = parser.parse_args()
        
# Load names of classes
classesFile = "classes.names";

classes = None
with open(classesFile, 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

print("class",classes)
# Give the configuration and weight files for the model and load the network using them.

modelConfiguration = "custom_logo/yolov3-tiny.cfg";
modelWeights = "backup/yolov3-tiny_18000.weights";

# modelConfiguration = "/content/gdrive/My Drive/darknet/custom/yolov3-tiny.cfg";
# modelWeights = "/content/gdrive/My Drive/darknet/backup/yolov3-tiny_5000.weights";

net = cv.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
print("done")
net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

# Get the names of the output layers
def getOutputsNames(net):
    # Get the names of all the layers in the network
    layersNames = net.getLayerNames()
    print("layer name",layersNames)
    # Get the names of the output layers, i.e. the layers with unconnected outputs
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]  #xác định lớp đầu ra là ['yolo_16', 'yolo_23']

# Draw the predicted bounding box
def drawPred(classId, conf, left, top, right, bottom):
    # Draw a bounding box.
    cv.rectangle(frame, (left, top), (right, bottom), (255, 178, 50), 3)
    # cv.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 3) #cv.rectangle vẽ hình chữ nhật

    label = '%.2f' % conf
        
    # Get the label for the class name and its confidence
    if classes:
        assert(classId < len(classes))
        label = '%s:%s' % (classes[classId], label)

    #Display the label at the top of the bounding box
    labelSize, baseLine = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    top = max(top, labelSize[1])
    # vẽ hộp giới hạn cho label
    cv.rectangle(frame, (left, top - round(1.5*labelSize[1])), (left + round(1.5*labelSize[0]), top + baseLine), (0, 0, 255), cv.FILLED)
    # cv.rectangle(frame, (left, top - round(1.5*labelSize[1])), (left + round(1.5*labelSize[0]), top + baseLine),    (255, 255, 255), cv.FILLED)
    #vẽ label
    cv.putText(frame, label, (left, top), cv.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,0), 2)

# Remove the bounding boxes with low confidence using non-maxima suppression
def postprocess(frame, outs):
    print("frame.shape",frame.shape)
    frameHeight = frame.shape[0]
    frameWidth = frame.shape[1]

    classIds = []
    confidences = []
    boxes = []
    # Scan through all the bounding boxes output from the network and keep only the
    # ones with high confidence scores. Assign the box's class label as the class with the highest score.
    classIds = []
    confidences = []
    boxes = []
    for out in outs:  # outs là ['yolo_16', 'yolo_23'] do yolo tiny chỉ sử dụng 2 layer đầu ra dự đoán, yolo3 sử dụng 3 layer
        print("out.shape : ", out.shape)
        '''
        out.shape :  (507, 8) lặp 507 lần ở đầu ra yolo 16 lúc này image bị chia thành 507 ô
        i 507
        out.shape :  (2028, 8) lặp 2028 lần ở đầu ra yolo 16
        i 2028
        '''
        i = 0
        for detection in out:
            i = i +1
            # print("detection",detection)
            #if detection[4]>0.001:
            scores = detection[5:]  #lấy từ phần tử thứ 5 về cuối để dự đoán
            # print("scores",scores)
            classId = np.argmax(scores) #tìm ra vị trí scores max
            # print("classId",classId)
            #if scores[classId]>confThreshold:
            confidence = scores[classId]   #số dự đoán
            # print("confidence",confidence)
            # if detection[4]>confThreshold:
                # print(detection[4], " - ", scores[classId], " - th : ", confThreshold)
                # print("detection")
            if confidence > confThreshold:
                print("true")
                print("scores",scores)
                print("classId",classId)
                print("confidence",confidence)
                '''
                true
                scores [0.         0.         0.99189883]
                classId 2
                confidence 0.99189883
                '''
                center_x = int(detection[0] * frameWidth) # detection[0] tọa độ width trung tâm của box 
                center_y = int(detection[1] * frameHeight)
                width = int(detection[2] * frameWidth)
                height = int(detection[3] * frameHeight)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                classIds.append(classId)
                confidences.append(float(confidence)) #score
                boxes.append([left, top, width, height])
        print("i",i)

    # Perform non maximum suppression to eliminate redundant overlapping boxes with
    # lower confidences.
    indices = cv.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)  #Threshold trên 0.5 thì giữ dưới thì xóa 
    for i in indices:
        print("i",i)
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        print("label",classes[classIds[i]])
        drawPred(classIds[i], confidences[i], left, top, left + width, top + height)

# Process inputs
# winName = 'Deep learning object detection in OpenCV'
# cv.namedWindow(winName, cv.WINDOW_NORMAL)

outputFile = "yolo_out_py.mp4"
if (args.image):
    # Open the image file
    if not os.path.isfile(args.image):
        # print("Input image file ", args.image, " doesn't exist")
        sys.exit(1)
    cap = cv.VideoCapture(args.image)
    outputFile = args.image[:-4]+'_yolo_out_py.jpg'
elif (args.video):
    # Open the video file
    if not os.path.isfile(args.video):
        # print("Input video file ", args.video, " doesn't exist")
        sys.exit(1)
    cap = cv.VideoCapture(args.video)
    outputFile =args.video[:-4]+'_yolo_out_py1.mp4'
else:
    # Webcam input
    cap = cv.VideoCapture(0)

# Get the video writer initialized to save the output video
if (not args.image):
    vid_writer = cv.VideoWriter(outputFile, cv.VideoWriter_fourcc('M','J','P','G'), 30, (round(cap.get(cv.CAP_PROP_FRAME_WIDTH)),round(cap.get(cv.CAP_PROP_FRAME_HEIGHT))))

while cv.waitKey(1) < 0:
    
    # get frame from the video
    hasFrame, frame = cap.read()
    
    # Stop the program if reached end of video
    if not hasFrame:
        # print("Done processing !!!")
        # print("Output file is stored as ", outputFile)
        cv.waitKey(3000)
        break

    print("frame",frame)

    # Create a 4D blob from a frame.
    blob = cv.dnn.blobFromImage(frame, 1/255, (inpWidth, inpHeight), [0,0,0], 1, crop=False)
    print("blob",blob)

    # Sets the input to the network
    net.setInput(blob)

    # Runs the forward pass to get output of the output layers
    # a = getOutputsNames(net)
    # print("a",a)
    outs = net.forward(getOutputsNames(net))   #lấy ra dự đoán 2 lớp đầu ra là ['yolo_16', 'yolo_23'] = vs outs = net.forward(['yolo_16', 'yolo_23'])
    # outs = net.forward(['yolo_16', 'yolo_23'])
    print("outs",outs)  #mảng 2 chiều 1 của yolo 16 1  của yolo23

    # Remove the bounding boxes with low confidence
    postprocess(frame, outs)

    # Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
    t, _ = net.getPerfProfile()
    label = 'Inference time: %.2f ms' % (t * 1000.0 / cv.getTickFrequency())
    #cv.putText(frame, label, (0, 15), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

    # Write the frame with the detection boxes
    if (args.image):
        cv.imwrite(outputFile, frame.astype(np.uint8));
    else:
        vid_writer.write(frame.astype(np.uint8))
    # cv.imshow(winName, frame)

timer = time.time() - before
print("thời gian chạy",timer)
    

