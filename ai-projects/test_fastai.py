from fastai import *
from fastai.text import *
from sklearn.metrics import f1_score

@np_func
def f1(inp,targ): return f1_score(targ, np.argmax(inp, axis=-1))

# data_path = Path('/content/drive/My Drive/fastai_NLP')
# name = 'viwiki'
path = '/var/www/html/demo.vinsofts.com/ai-projects'
bs = 32
data_clas = load_data(path,'vi_textlist_class', bs=bs, num_workers=1)
learn_c = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5, metrics=[accuracy,f1]).to_fp16()
a = learn_c.load('vi_clas', purge=False)
result = a.predict("chán")
print("result",result)
