#!/usr/bin/env python 
# -*- coding:utf-8 -*- 
######## Image Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/15/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier uses it to perform object detection on an image.
# It draws boxes and scores around the objects of interest in the image.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import urllib.request
import requests
import json
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
# Import utilites
from utils import label_map_util_sach
from utils import visualization_utils_sach as vis_util_sach
from utils import label_map_util_thuoc
from utils import visualization_utils_thuoc as vis_util_thuoc
from flask import Flask, jsonify, request
from flask_cors import CORS

import pytesseract
import re

app = Flask(__name__)
# app.config['SERVER_NAME'] = '124.0.0.1:5555'
# app.config.from_pyfile('config.cfg')
CORS(app)

@app.route('/comment/<path:link>',methods = ['GET'])
# print(link)
def returnComment(link):
    import numpy as np
    import pandas as pd
    from keras.models import Model
    # from keras.layers import Input, Dense, Embedding, SpatialDropout1D, Dropout, add, concatenate
    # from keras.layers import LSTM, Bidirectional, GlobalMaxPooling1D, GlobalAveragePooling1D
    from keras.preprocessing import text, sequence
    # from keras.callbacks import LearningRateScheduler
    from keras.losses import binary_crossentropy
    from keras import backend as K
    from keras.models import load_model
    # from keras.losses import mean_squared_abs_error



    def custom_loss(y_true, y_pred):
        return binary_crossentropy(K.reshape(y_true[:,0],(-1,1)), y_pred) * y_true[:,1]

    def preprocess(data):
        '''
        Credit goes to https://www.kaggle.com/gpreda/jigsaw-fast-compact-solution
        '''
        punct = "/-'?!.,#$%\'()*+-/:;<=>@[\\]^_`{|}~`" + '""“”’' + '∞θ÷α•à−β∅³π‘₹´°£€\×™√²—–&'
        def clean_special_chars(text, punct):
            for p in punct:
                text = text.replace(p, ' ')
            return text

        data = data.astype(str).apply(lambda x: clean_special_chars(x, punct))
        return data

    MAX_LEN = 220
    train = pd.read_csv('train.csv')
    test = pd.read_csv('test.csv')
    # print("test",test)
    x_train = preprocess(train['comment_text'])
    x_testt = preprocess(test['comment_text'])
    # link = "fuck you"
    x_test = pd.DataFrame([link])
    x_test = x_test.iloc[0]
    x_test = preprocess(x_test)
    tokenizer = text.Tokenizer()
    # tokenizer.fit_on_texts(list(x_test))
    tokenizer.fit_on_texts(list(x_train) + list(x_testt))
    print("tokenizer",tokenizer)
    x_test = tokenizer.texts_to_sequences(x_test)
    x_test = sequence.pad_sequences(x_test, maxlen=MAX_LEN) 

    print("x_test",x_test.shape)
    model = load_model("model.h5",custom_objects={'custom_loss':custom_loss})

    predict = model.predict(x_test, batch_size=2048)[0].flatten()
    print("predict",predict[0])
    if predict[0] > 0.5:
        return jsonify(["Toxicity"])
    else:
        return jsonify(["No toxicity"])

if __name__ == "__main__":
    app.run(debug=True,port=8080)
    # app.config['SERVER_NAME'] = "127.0.0.1:8080"
    # app.run()
#run http://127.0.0.1:8080/image/

