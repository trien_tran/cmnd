<?php include('../header.php') ?>  
<body class="royal_preloader">  
  
  <div id="royal_preloader"></div>
  
  <!-- MENU
    ================================================== -->  

    <nav id="menu-wrap" class="menu-back cbp-af-header">
      <div class="parallax-pattern" style="background-image: url('images/parallax-pattern.jpg')"></div>
      <?php include('../menu-top.php') ?>
    </nav>
  
    
  <!-- Primary Page Layout
  ================================================== -->

    <div class="section-block small-height">
        <div class="parallax" style="background-image: url('https://www.macobserver.com/wp-content/uploads/2018/04/AI-concept-human-head-1200x632.jpg')"></div>
        <div class="dark-over-hero"></div>
        
        <div class="home-text-freelance project-hero-margin z-bigger">
          <div class="container fade-elements">
            <div class="twelve columns">
              <h2>Nhận diện thương hiệu</h2>
            </div>
          </div>
        </div>
          
    </div>
    <br>


    <div>
      <p style="font-size: 24px;text-align: center;">1. Giới thiệu về ứng dụng:</p>
    <br>
    <br>
      <p style="font-size: 24px;text-align: center;">2. Hình ảnh/Video mô phỏng kết quả:</p>
      <br>
      <br>
      <br>
      <!-- <video width="320" height="240" controls> -->
      <!-- <source src="detect_logo_yolo/test_video/video_adidas_yolo_out_py.mp4" type="video/mp4"> -->
      <!-- <source src="movie.ogg" type="video/ogg"> -->
      <!-- </video> -->
      <!-- <iframe style="margin: 0 auto; display: block;" width="450" height="283" src="video_demo/demo_violence.mp4"  frameborder="0" allowfullscreen wmode="Opaque"></iframe> -->
      <iframe style="margin: 0 auto; display: block;" width="450" height="283" src="video_demo/logo.mp4"  frameborder="0" allowfullscreen wmode="Opaque"></iframe>
      <!-- <embed src="result/yolo_out_py.avi"/> -->
      <br>
      <br>
    <br>
    <br>
      <p style="font-size: 24px;text-align: center;">3. Kiểm thử ứng dụng</p>
    </div>
    
    <div class="section-block padding-top-bottom">
    <script>
            $(document).ready(function(){
                $("#but_upload").click(function(){
                  var fd = new FormData();
                  var files = $('#file')[0].files[0];
                  fd.append('file',files);
                  // $(".btn-detect-img").hide();


                  $.ajax({
                      url: 'upload.php',
                      type: 'post',
                      data: fd,
                      contentType: false,
                      processData: false,
                      beforeSend: function () { // traitements JS à faire AVANT l'envoi
                          $('#ajaxLoader-inner')
                          .html('<img src="https://cdn.dribbble.com/users/178981/screenshots/2245419/xxxx.gif" /> <span>Vui lòng đợi...</span>'); // add a gif loader  
                      },
                      success: function(response){
                          if(response != 0){
                              $("#img").attr("src",response); 
                              $(".preview img").show(); // Display image element
                              // var url = 'http://127.0.0.1:8080/logo/';
                              var url = 'http://159.65.135.95:8080/logo/';
                              var getUrl = window.location;
                              var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
                              var link = response; 
                              // var link = response; 
                              // $("#video-violence").attr("src",response)
                              $("#video-violence").html('<iframe style="margin: 0 auto; display: block;" width="450" height="283" src="'+response+'"  frameborder="0" allowfullscreen wmode="Opaque"></iframe>');
                              console.log(response);

                              $.get(url + link , function(data) {
                                console.log(data[0])
                                $('#ajaxLoader-inner img, #ajaxLoader-inner span').fadeOut(0000, function () { })
                                $("#video-violence_train").html('<iframe style="margin: 0 auto; display: block;" width="450" height="283" src="'+data[0]+'"  frameborder="0" allowfullscreen wmode="Opaque"></iframe>');
                                // $("#video-violencee").attr("src",data[0]); 
                              })
                                // })
                          }else{
                              alert('file not uploaded');
                          }
                      },
                  });
              });
                // $("#video-violence").attr("src","uploads/violence_tram_xang_4.mp4")
          });

        </script>
      <!-- http://media.anhp.vn:8081/files/nganpham/NGAN_501527388674.jpg -->
      <div id="form-train-detect">
        <div class="container khung" id="video-violence">
          <!-- <embed src="" id="video-violence"> -->
            <!-- <iframe id="video-violence" width="450" height="283" src="" frameborder="0" allowfullscreen wmode="Opaque"></iframe> -->
        </div>
        <br>
        <div class="container khung" id="video-violence_train">
        
        </div>
        <div class="container khung">
                <div id="ajaxLoader-inner" style="display: inline-block;width: 100%;text-align: center;color: #545050;"></div>
                <div class="image-show"></div>    
        </div>

        <div class="container1">
          <form method="post" action="/upload.php" enctype="multipart/form-data" id="myform">
              <div >
                  <input type="file" id="file" name="file" /><br><br>
                  <input style="width: 200px" type="button" class="button" value="Upload" id="but_upload">
              </div>
          </form>
        </div>
              
        </div>
      </div>
  <!-- </div> -->
  
    <?php include '../footer-page.php';?>