<?php include('../header.php') ?> 

<body class="royal_preloader">	
	
	<div id="royal_preloader"></div>
	
	<!-- MENU
    ================================================== -->	

		<nav id="menu-wrap" class="menu-back cbp-af-header">
      <div class="parallax-pattern" style="background-image: url('images/parallax-pattern.jpg')"></div>
      <?php include('../menu-top.php') ?>
    </nav>
	
		
	<!-- Primary Page Layout
	================================================== -->

	<div class="section-block small-height">
		<div class="parallax" style="background-image: url('https://www.macobserver.com/wp-content/uploads/2018/04/AI-concept-human-head-1200x632.jpg')"></div>
		<div class="dark-over-hero"></div>
		
		<div class="home-text-freelance project-hero-margin z-bigger">
			<div class="container fade-elements">
				<!-- <div class="twelve columns">
					<h2>Disrepute<br><span>illustration, graphic design</span></h2>
				</div> -->
			</div>
		</div>
			
	</div>
		
	<div class="section-block background-color-blue" id="top-scroll">
		<style>
			.content__link .chaffle {
			    width: 50px;
			    height: auto;
			    -moz-border-radius: 100px / 50px;
			    -webkit-border-radius: 100px / 50px;
			    border-radius: 100px / 50px;
			    text-align: center;
			    color: white;
			    background-color: black;
			    padding: 10px 20px;
			}
		</style>
		<div class="morph-wrap">
			<svg class="morph" width="1400" height="770" viewBox="0 0 1400 770">
				<path d="M 262.9,252.2 C 210.1,338.2 212.6,487.6 288.8,553.9 372.2,626.5 511.2,517.8 620.3,536.3 750.6,558.4 860.3,723 987.3,686.5 1089,657.3 1168,534.7 1173,429.2 1178,313.7 1096,189.1 995.1,130.7 852.1,47.07 658.8,78.95 498.1,119.2 410.7,141.1 322.6,154.8 262.9,252.2 Z"/>
			</svg>
		</div>
		<div class="content-wrap padding-top-bottom content--layout-1">
			<div class="content content--layout" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
			<a href="/ai-projects/detectsach.php">				
				<img class="content__img" src="video_demo/demo_sach.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện đối tượng sách trong hình ảnh</h3> -->
				<a href="/ai-projects/detectsach.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle"><p>Xem chi tiết</p></div></a>
			</div>
			<div class="content content--layout mobile-padding" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
				<a href="/ai-projects/detectsach.php">
				<img class="content__img" src="video_demo/demo_thuoc.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện thông tin thuốc trong hình ảnh đơn thuốc</h3> -->
				<a href="/ai-projects/detectthuoc.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle"><p>Xem chi tiết</p></div></a>
			</div>
		</div>
		<div class="content-wrap padding-bottom content--layout-2">
			<div class="content content--layout" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
				<a href="/ai-projects/detectsach.php">
				<img class="content__img" src="video_demo/demo_comment.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện nội dung comment vi phạm</h3> -->
				<a href="/ai-projects/detectcomenttoxic.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle"><p>Xem chi tiết</p></div></a>
			</div>
			<div class="content content--layout mobile-padding" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
				<a href="/ai-projects/detectviolence.php">
				<img class="content__img" src="video_demo/demo_violence.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện hành vi đánh nhau</h3> -->
				<a href="/ai-projects/detectviolence.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle" ><p>Xem chi tiết</p></div></a>
			</div>
		</div>

		<div class="content-wrap padding-bottom content--layout-2">
			<div class="content content--layout" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
				<a href="/ai-projects/detectcomenttoxicVietNames.php">
				<img class="content__img" src="video_demo/demo_comment_vie.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện nội dung comment vi phạm</h3> -->
				<a href="/ai-projects/detectcomenttoxicVietNames.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle"><p>Xem chi tiết</p></div></a>
			</div>
			<div class="content content--layout mobile-padding" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
				<a href="/ai-projects/detectlogo.php">
				<img class="content__img" src="video_demo/demo_logo.png" alt="Some image" /></a>
				<!-- <h3 class="content__title">Nhận diện thương hiệu</h3> -->
				<a href="/ai-projects/detectlogo.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle" ><p>Xem chi tiết</p></div></a>
			</div>
		</div>


		<div class="content-wrap padding-top-bottom content--layout-1">
			<div class="content content--layout" data-scroll-reveal="enter bottom move 60px over 0.9s after 0.1s">
				<div class="content__mask"></div>
			<a href="/ai-projects/detectsach.php">				
				<img class="content__img" src="video_demo/demo_cmnd.png" alt="Some image" /></a>
				<a href="/ai-projects/detect_cmnd.php" class="content__link"><svg class="decoshape" width="100%" height="100%" preserveAspectRatio="none"><use xlink:href="#shape"></use></svg><div class="chaffle"><p>Xem chi tiết</p></div></a>
			</div>
		</div>


	</div>
	
	  <?php include '../footer-page.php';?>