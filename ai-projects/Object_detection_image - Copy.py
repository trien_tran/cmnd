#!/usr/bin/env python 
# -*- coding:utf-8 -*- 
######## Image Object Detection Using Tensorflow-trained Classifier #########
#
# Author: Evan Juras
# Date: 1/15/18
# Description: 
# This program uses a TensorFlow-trained classifier to perform object detection.
# It loads the classifier uses it to perform object detection on an image.
# It draws boxes and scores around the objects of interest in the image.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
import urllib.request
import requests
import json
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
# Import utilites
from utils import label_map_util_sach
from utils import visualization_utils_sach as vis_util_sach
from utils import label_map_util_thuoc
from utils import visualization_utils_thuoc as vis_util_thuoc
from flask import Flask, jsonify, request
from flask_cors import CORS

import pytesseract
import re

app = Flask(__name__)
# app.config['SERVER_NAME'] = '124.0.0.1:5555'
# app.config.from_pyfile('config.cfg')
CORS(app)


@app.route('/image/<path:link>',methods = ['GET'])
# print(link)
def returnSach(link):
    print("link",link)
    MODEL_NAME = 'trained-inference-graphs/output_inference_graph_v1.pb'
    # IMAGE_NAME = '/home/ttest_images/image8.jpg'

    # url = requests.get('http://127.0.0.1:8082/image').json()["url"]         #lấy ra url
    # print (url)
    # req = urllib.request.urlopen(link)
    # arr = np.asarray(bytearray(req.read()), dtype=np.uint8)                 #dua hinh anh ve [255 216 255 ... 215 255 217] de doc
    # Grab path to current working directory
    CWD_PATH = os.getcwd()
    PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join(CWD_PATH,'training','label_map.pbtxt')  #khong can CWD_PATH cung dc :))

    # Path to image
    # PATH_TO_IMAGE = os.path.join(CWD_PATH,IMAGE_NAME)
    # PATH_TO_IMAGE = arr
    # Number of classes the object detector can identify
    NUM_CLASSES = 10                                                     # chú ý thay đổi

    # đọc file label_map.pbtxt
    label_map = label_map_util_sach.load_labelmap(PATH_TO_LABELS)
    # chuyển object label_map trên thành 1 mảng các dict [{'id': 1, 'name': u'sachh'}, {'id': 2, 'name': u'coc'}]
    categories = label_map_util_sach.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    # chuyển mảng categories trên thành 1 list kiểu dict {1: {'id': 1, 'name': u'sachh'}, 2: {'id': 2, 'name': u'coc'}}
    category_index = label_map_util_sach.create_category_index(categories)
    # Load the Tensorflow model into memory.(Tải mô hình Tensorflow vào bộ nhớ)
    detection_graph = tf.Graph()
    with detection_graph.as_default(): #tạo 1 graph mặc định
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()   #Trả về nội dung của tệp dưới dạng chuỗi ký tư mã hóa.
            od_graph_def.ParseFromString(serialized_graph)  # paser chuỗi trên về string 
            tf.import_graph_def(od_graph_def, name='')       # nhúng chuỗi trên vào graph

        sess = tf.Session(graph=detection_graph)
    # Define input and output tensors (i.e. data) for the object detection classifier (Xác định các tensor đầu vào và đầu ra (tức là dữ liệu) cho trình phân loại phát hiện đối tượng)

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    # Output tensors are the detection boxes, scores, and classes  (Các thang đo đầu ra là các hộp phát hiện, điểm số và các lớp)
    # Each box represents a part of the image where a particular object was detected (Mỗi hộp đại diện cho một phần của hình ảnh nơi phát hiện một đối tượng cụ thể)
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects. (Mỗi điểm thể hiện mức độ tự tin cho từng đối tượng)
    # The score is shown on the result image, together with the class label.  (Điểm được hiển thị trên hình ảnh kết quả, cùng với nhãn lớp.)
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected (Số lượng đối tượng được phát hiện)
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Load image using OpenCV and
    # expand image dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value


    # Tải hình ảnh bằng OpenCV và
    # mở rộng kích thước hình ảnh để có hình dạng: [1, Không có, Không có, 3]
    # tức là một mảng một cột, trong đó mỗi mục trong cột có giá trị RGB pixel

    image = cv2.imread(link)
    # image = cv2.imdecode(PATH_TO_IMAGE, -1)
    image_expanded = np.expand_dims(image, axis=0)  # mở rộng mảng theo trục x
    """
    x = np.array([1,2])
    y = np.expand_dims(x, axis=0)
    print(y)
    array([[1, 2]])
    """

    # Perform the actual detection by running the model with the image as input(Thực hiện phát hiện thực tế bằng cách chạy mô hình với hình ảnh làm đầu vào)
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    # Draw the results of the detection (aka 'visulaize the results') (Vẽ kết quả phát hiện (còn gọi là 'hiển thị kết quả'))

    place = vis_util_sach.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8,
        min_score_thresh=0.80)
    # places = json.dumps(place)
    # cv2.imshow('Object detector', image)
    return jsonify(place)


@app.route('/thuoc/<path:link>',methods = ['GET']) 
    # code upload anh

def returnThuoc(link):
    listLink = link.split("-",1)
    print("listLink",listLink)
    MODEL_NAME = ""
    if listLink[0] == "bcBachMai":
        MODEL_NAME = 'D:/Tensorflow/workspace/training_demo_don_thuoc_bv_da_khoa_SG/trained-inference-graphs/output_inference_graph_v18947.pb'

    PATH_TO_CKPT = os.path.join(MODEL_NAME,'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join('label_map_thuoc.pbtxt')  #khong can CWD_PATH cung dc :))

    NUM_CLASSES = 3

    label_map = label_map_util_thuoc.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util_thuoc.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util_thuoc.create_category_index(categories)

    # Load the Tensorflow model into memory.(Tải mô hình Tensorflow vào bộ nhớ)
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier (Xác định các tensor đầu vào và đầu ra (tức là dữ liệu) cho trình phân loại phát hiện đối tượng)

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes  (Các thang đo đầu ra là các hộp phát hiện, điểm số và các lớp)
    # Each box represents a part of the image where a particular object was detected (Mỗi hộp đại diện cho một phần của hình ảnh nơi phát hiện một đối tượng cụ thể)
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects. (Mỗi điểm thể hiện mức độ tự tin cho từng đối tượng)
    # The score is shown on the result image, together with the class label.  (Điểm được hiển thị trên hình ảnh kết quả, cùng với nhãn lớp.)
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected (Số lượng đối tượng được phát hiện)
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Load image using OpenCV and
    # expand image dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value


    # Tải hình ảnh bằng OpenCV và
    # mở rộng kích thước hình ảnh để có hình dạng: [1, Không có, Không có, 3]
    # tức là một mảng một cột, trong đó mỗi mục trong cột có giá trị RGB pixel

    image = cv2.imread(listLink[1])
    # image = cv2.imdecode(PATH_TO_IMAGE, -1)
    orig = image.copy()
    print("orig",orig.shape)
    image_expanded = np.expand_dims(image, axis=0)
    # print("image_expanded",image_expanded)
    # Perform the actual detection by running the model with the image as input(Thực hiện phát hiện thực tế bằng cách chạy mô hình với hình ảnh làm đầu vào)
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_expanded})

    # Draw the results of the detection (aka 'visulaize the results') (Vẽ kết quả phát hiện (còn gọi là 'hiển thị kết quả'))
    # print("np.squeeze(boxes)",np.squeeze(boxes))
    # print("np.squeeze(classes).astype(np.int32)",np.squeeze(classes).astype(np.int32))
    # print("np.squeeze(scores)",np.squeeze(scores))
    # print("np.squeeze(boxes)",np.squeeze(boxes))

    image,box_crop = vis_util_thuoc.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8,
        min_score_thresh=0.80)
    print("box_crop",box_crop)  #tọa độ detect thuốc
    # cv2.imshow("sds",image)
    # cv2.waitKey(0)
    # cắt ảnh theo tọa độ và dùng tesseract để detect text
    height,width,z = orig.shape   #lấy ra chiều cáo, rộng,kênh màu của ảnh

    list_end1 = []
    for box,color in box_crop:
        list = []
        list1 = []
        list_end = []
        ymin,xmin,ymax,xmax = box       #lấy ra tọa độ detect box 
        xmin_crop = int(xmin*width)
        # print("xmin_crop",xmin_crop)
        xmax_crop = int(xmax*width)
        # print("xmax_crop",xmax_crop)
        ymin_crop = int(ymin*height)
        # print("ymin_crop",ymin_crop)
        ymax_crop = int(ymax*height)
        # print("ymax_crop",ymax_crop)
        roi = orig[ymin_crop:ymax_crop, xmin_crop:xmax_crop]  #(ymin:ymax,xmin:xmax) ảnh sau khi crop
        crop_name = 'out1.jpg'
        cv2.imwrite(crop_name, roi)
        # cv2.imshow("sds",roi)
        # cv2.waitKey(0)
        # dùng tesseract
        config = ('-l eng+vie --oem 1 --psm 11')
        # config = ('-l eng --oem 1 --psm 6')
        # config = ('-l eng --oem 1 --psm 12')
        # config = ('-l eng+vie --oem 1 --psm 3')
        text = pytesseract.image_to_string(roi, config=config)
        print("text",text)
        text = re.sub('[^a-zA-Z\n()1-9 ]', '', text)
        text = text.split("\n")
        print("text1",text)

    # list_end2 = []
        for x in text:
            check = 0
            if not x:
                continue
            else:
                for y in x:
                    if y == "(" or y == ")":
                        check += 1
                if check == 0:
                    list.append(x.split())
                check = 0 

        # print("list",list)

        for x in list:
            if not x:
                continue
            else:
                for y in x:
                    if not y:
                        continue
                    elif len(y)> 4:
                        list_end.append(y)
    # print("list_end",list_end)
    # Clean up
        for x in list_end:
            if x[0].isupper() != False and sum(c.isupper() for c in x) < 3: #x[0].isupper() != False and chữ cái đầu la in hoa
                list_end1.append(x)

    print("list_end1",list_end1)

    cv2.destroyAllWindows()
    return jsonify(list_end1)


@app.route('/comment/<path:link>',methods = ['GET'])
def returnComment(link):
    import numpy as np
    import pandas as pd
    from keras.models import Model
    # from keras.layers import Input, Dense, Embedding, SpatialDropout1D, Dropout, add, concatenate
    # from keras.layers import LSTM, Bidirectional, GlobalMaxPooling1D, GlobalAveragePooling1D
    from keras.preprocessing import text, sequence
    # from keras.callbacks import LearningRateScheduler
    from keras.losses import binary_crossentropy
    from keras import backend as K
    from keras.models import load_model
    # from keras.losses import mean_squared_abs_error



    def custom_loss(y_true, y_pred):
        return binary_crossentropy(K.reshape(y_true[:,0],(-1,1)), y_pred) * y_true[:,1]

    def preprocess(data):
        '''
        Credit goes to https://www.kaggle.com/gpreda/jigsaw-fast-compact-solution
        '''
        punct = "/-'?!.,#$%\'()*+-/:;<=>@[\\]^_`{|}~`" + '""“”’' + '∞θ÷α•à−β∅³π‘₹´°£€\×™√²—–&'
        def clean_special_chars(text, punct):
            for p in punct:
                text = text.replace(p, ' ')
            return text

        data = data.astype(str).apply(lambda x: clean_special_chars(x, punct))
        return data

    MAX_LEN = 220
    train = pd.read_csv('train.csv')
    test = pd.read_csv('test.csv')
    # print("test",test)
    x_train = preprocess(train['comment_text'])
    x_testt = preprocess(test['comment_text'])
    # link = "fuck you"
    x_test = pd.DataFrame([link]) #chuyen str thành  DataFrame
    x_test = x_test.iloc[0]
    x_test = preprocess(x_test)
    tokenizer = text.Tokenizer()
    # tokenizer.fit_on_texts(list(x_test))
    tokenizer.fit_on_texts(list(x_train) + list(x_testt))
    print("tokenizer",tokenizer)
    x_test = tokenizer.texts_to_sequences(x_test)
    x_test = sequence.pad_sequences(x_test, maxlen=MAX_LEN) 

    print("x_test",x_test.shape)
    model = load_model("model.h5",custom_objects={'custom_loss':custom_loss})

    predict = model.predict(x_test, batch_size=2048)[0].flatten()
    print("predict",predict[0])
    if predict[0] > 0.5:
        return jsonify(["Toxicity"])
    else:
        return jsonify(["No toxicity"])

@app.route('/violence/<path:link>',methods = ['GET'])
def returnViolence(link):
    sys.path.append("..")

    # Import utilites
    from utils import label_map_util_violence
    from utils import visualization_utils_violence as vis_util_violence

    # Name of the directory containing the object detection module we're using
    # MODEL_NAME = 'inference_graph'
    MODEL_NAME = 'output_inference_graph_v2.pb'
    # MODEL_NAME = 'C:/Users/anlan/OneDrive/Desktop/output_inference_graph_v2.pb'
    # VIDEO_NAME = 'C:/Users/anlan/OneDrive/Desktop/frame_violence/violence_nha_tu1.mp4'
    # VIDEO_NAME = 'C:/Users/anlan/OneDrive/Desktop/frame_violence/violence_tram_xang.mp4'
    # VIDEO_NAME = 'C:/Users/anlan/Downloads/Telegram Desktop/demo_nha_tu.mp4'
    url = link.split("/",1)
    print("url",url)

    # Grab path to current working directory
    CWD_PATH = os.getcwd()

    # Path to frozen detection graph .pb file, which contains the model that is used
    # for object detection.
    PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

    # Path to label map file
    PATH_TO_LABELS = os.path.join('label_map.pbtxt')

    # Path to video
    PATH_TO_VIDEO = os.path.join(CWD_PATH,url[1])

    # Number of classes the object detector can identify
    NUM_CLASSES = 2

    # Load the label map.
    # Label maps map indices to category names, so that when our convolution
    # network predicts `5`, we know that this corresponds to `king`.
    # Here we use internal utility functions, but anything that returns a
    # dictionary mapping integers to appropriate string labels would be fine
    label_map = label_map_util_violence.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util_violence.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util_violence.create_category_index(categories)


    # class VideoSavor:
    # 	def AppendFrame(self, image_):
    # 		self.outputStream.write(image_)

    # 	def __init__(self, targetFileName, videoCapture):
    # 		width = int(500)
    # 		height = int(500)
    # 		frameRate = int( videoCapture.get(cv2.CAP_PROP_FPS) )
    # 		codec = cv2.VideoWriter_fourcc(*'XVID')
    # 		self.outputStream = cv2.VideoWriter(targetFileName + ".avi", codec, frameRate, (width, height) )



    # Load the Tensorflow model into memory.
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

        sess = tf.Session(graph=detection_graph)

    # Define input and output tensors (i.e. data) for the object detection classifier

    # Input tensor is the image
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Output tensors are the detection boxes, scores, and classes
    # Each box represents a part of the image where a particular object was detected
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represents level of confidence for each of the objects.
    # The score is shown on the result image, together with the class label.
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

    # Number of objects detected
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    PATH_FILE_NAME_TO_SAVE_RESULT = "result/output8.mp4"
    # Open video file

    video = cv2.VideoCapture(PATH_TO_VIDEO)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(PATH_FILE_NAME_TO_SAVE_RESULT,fourcc, 20.0,(int(video.get(3)), int(video.get(4))))  #(int(video.get(3)), int(video.get(4) chiều cao,rông của input = chiều cao rộng của output
    # videoSavor = VideoSavor(PATH_FILE_NAME_TO_SAVE_RESULT + "_Result", video) # luu video
    while(video.isOpened()):

        # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
        # i.e. a single-column array, where each item in the column has the pixel RGB value
        ret, frame = video.read()
        frame_expanded = np.expand_dims(frame, axis=0)
        if frame_expanded[0] is None:
            break;
        # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: frame_expanded})
        # print("np.squeeze(classes).astype(np.int32)",np.squeeze(classes).astype(np.int32))
        # print("category_index",category_index)
        # print([category_index.get(i) for i in classes[0]])
        # print(scores)
        # Draw the results of the detection (aka 'visulaize the results')
        place = vis_util_violence.visualize_boxes_and_labels_on_image_array(
            frame,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=8,
            min_score_thresh=0.80)
        out.write(frame)
        print("place",place)
        # print("frame",frame)
        # All the results have been drawn on the frame, so it's time to display it.
        cv2.imshow('Object detector', frame)
        # videoSavor.AppendFrame(frame)
        # Press 'q' to quit
        # print("dasdadas")
        if cv2.waitKey(1) == ord('q'):
            break
    # print("dadadadada")
    # Clean up
    video.release()
    cv2.destroyAllWindows()
    return jsonify(["result/output8.mp4"])




if __name__ == "__main__":
    app.run(debug=True,port=8080)
    # app.config['SERVER_NAME'] = "127.0.0.1:8080"
    # app.run()
#run http://127.0.0.1:8080/image/

